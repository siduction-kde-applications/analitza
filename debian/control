Source: analitza
Section: libs
Priority: optional
Maintainer: Debian/Kubuntu Qt/KDE Maintainers <debian-qt-kde@lists.debian.org>
Uploaders: Sune Vuorela <sune@debian.org>, Maximiliano Curia <maxy@debian.org>
Build-Depends: cmake (>= 2.8.12),
               debhelper (>= 9),
               extra-cmake-modules (>= 1.3.0),
               libeigen3-dev,
               libglew-dev,
               libncurses5-dev,
               libqt5opengl5-dev (>= 5.4),
               libqt5svg5-dev (>= 5.4),
               libreadline-dev,
               pkg-config,
               pkg-kde-tools (>> 0.15.15),
               qtbase5-dev (>= 5.4),
               qtdeclarative5-dev (>= 5.4)
Standards-Version: 3.9.6
XS-Testsuite: autopkgtest
Homepage: http://edu.kde.org/
Vcs-Browser: http://anonscm.debian.org/cgit/pkg-kde/applications/analitza.git
Vcs-Git: git://anonscm.debian.org/pkg-kde/applications/analitza.git

Package: analitza-common
Architecture: all
Depends: ${misc:Depends}
Replaces: libanalitzaplot4 (<< 4:4.12.90), libanalitzaplot5 (<< 4:4.12.90)
Conflicts: libanalitzaplot4 (<< 4:4.12.90), libanalitzaplot5 (<< 4:4.12.90)
Description: common files for Analitza
 Analitza is a library to parse and work with mathematical expressions. This
 library is being used by KAlgebra and Cantor and may be used in other
 programs.
 .
 This package is part of the KDE education module.

Package: libanalitza-dev
Architecture: any
Section: libdevel
Conflicts: kalgebra-dev
Depends: libanalitza6 (= ${binary:Version}),
         libanalitzagui6 (= ${binary:Version}),
         libanalitzaplot6 (= ${binary:Version}),
         libanalitzawidgets6 (= ${binary:Version}),
         qml-module-org-kde-analitza (= ${binary:Version}),
         qtbase5-dev (>= 5.4),
         ${misc:Depends}
Description: development files for Analitza
 Analitza is a library to parse and work with mathematical expressions. This
 library is being used by KAlgebra and Cantor and may be used in other
 programs.
 .
 This package contains the development files, used to build applications that
 use Analitza.
 .
 This package is part of the KDE education module.

Package: libanalitza6
Architecture: any
Depends: ${misc:Depends}, ${shlibs:Depends}
Description: library to work with mathematical expressions
 This library is used by KAlgebra and may be used by other software to parse and
 work with mathematical expressions.
 .
 This package is part of the KDE education module.

Package: libanalitzawidgets6
Architecture: any
Depends: ${misc:Depends}, ${shlibs:Depends}
Description: library to work with mathematical expressions - widgets
 This library is used by KAlgebra and may be used by other software to parse and
 work with mathematical expressions.
 .
 This package is part of the KDE education module.

Package: libanalitzagui6
Architecture: any
Depends: ${misc:Depends}, ${shlibs:Depends}
Breaks: kalgebra (<< 4:4.7.2)
Replaces: kalgebra (<< 4:4.7.2)
Description: library to work with mathematical expressions - GUI routines
 This library is used by KAlgebra and may be used by other software to parse and
 work with mathematical expressions. This library provides the routines related
 to the graphical user interface.
 .
 This package is part of the KDE education module.

Package: libanalitzaplot6
Architecture: any
Depends: analitza-common, ${misc:Depends}, ${shlibs:Depends}
Breaks: kalgebra (<< 4:4.7.2)
Replaces: kalgebra (<< 4:4.7.2)
Description: library to work with mathematical expressions - plotting bits
 This library is used by KAlgebra and may be used by other software to parse and
 work with mathematical expressions. This library provides the routines related
 to plotting.
 .
 This package is part of the KDE education module.

Package: qml-module-org-kde-analitza
Architecture: any
Depends: analitza-common, ${misc:Depends}, ${shlibs:Depends}
Description: library to work with mathematical expressions - QML library
 This library is used by KAlgebra and may be used by other software to parse and
 work with mathematical expressions. This library provides the routines related
 to plotting.
 .
 This package is part of the KDE education module.

Package: libanalitza-dbg
Architecture: any
Section: debug
Priority: extra
Depends: libanalitza6 (= ${binary:Version}),
         libanalitzagui6 (= ${binary:Version}),
         libanalitzaplot6 (= ${binary:Version}),
         ${misc:Depends}
Breaks: kalgebra-dbg (<< 4:4.8), kdeedu-dbg (<< 4:4.6.80)
Replaces: kalgebra-dbg (<< 4:4.8), kdeedu-dbg (<< 4:4.6.80)
Description: debugging symbols for Analitza
 This package contains debugging files used to investigate problems with
 libraries and binaries included in Analitza.
 .
 This package is part of the KDE education module.
